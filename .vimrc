set nocompatible              " be iMproved, required
filetype off

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" " alternatively, pass a path where Vundle should install plugins
" "call vundle#begin('~/some/path/here')

" " let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
Plugin 'ervandew/supertab'

Plugin 'Valloric/YouCompleteMe' "After install, follow:
"https://github.com/Valloric/YouCompleteMe
"
Plugin 'altercation/vim-colors-solarized' "After install, follow:
"http://www.webupd8.org/2011/04/solarized-must-have-color-paletter-for.html

Plugin 'bling/vim-airline'
Plugin 'jmcantrell/vim-virtualenv'
Plugin 'othree/html5.vim'
Plugin 'plasticboy/vim-markdown'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'
Plugin 'tpope/vim-fugitive'
Plugin 'godlygeek/tabular'

call vundle#end()            " required

syntax enable
filetype plugin indent on

set pastetoggle=<F2>
set number
set laststatus=2
set statusline=%{fugitive#statusline()},%{virtualenv#statusline()}

let g:virtualenv_stl_format = '[%n]'
let g:ycm_autoclose_preview_window_after_completion=1
let g:ycm_collect_identifiers_from_tags_files = 1
let g:ycm_min_num_of_chars_for_completion = 0
let g:ycm_seed_identifiers_with_syntax = 1
let g:syntastic_mode_map = { 'mode': 'passive' }
let g:syntastic_auto_loc_list=1     
let g:syntastic_python_checkers=['pylint']
let g:syntastic_python_checker_args='--ignore=D10'
nnoremap <silent> <F5> :SyntasticCheck<CR>  

" Visual
set showmatch  " Show matching brackets.
set mat=5  " Bracket blinking.
set background=dark
set t_Co=16
let g:solarized_termtrans = 1
let g:solarized_visibility = "high"
let g:solarized_contrast = "high"
colorscheme solarized
set guifont=Droid\ Sans\ Mono\ for\ Powerline\ 10 
let g:Powerline_symbols = 'fancy'
" Tab settings
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab

" NERD_tree Plugin Configuration
let NERDTreeChDirMode=2
let NERDTreeIgnore=['\.vim$', '\~$', '\.pyc$', '\.swp$']
let NERDTreeSortOrder=['^__\.py$', '\/$', '*', '\.swp$',  '\~$']
let NERDTreeShowBookmarks=1
map <F3> :NERDTreeToggle<CR>

" Syntax for multiple tag files are
" set tags=/my/dir1/tags, /my/dir2/tags
set tags=tags;$HOME/.vim/tags/

" Viewport Controls
" ie moving between split panes
map <silent>,h <C-w>h
map <silent>,j <C-w>j
map <silent>,k <C-w>k
map <silent>,l <C-w>l

" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-n>'

" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"

" " Brief help
" " :PluginList       - lists configured plugins
" " :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" " :PluginSearch foo - searches for foo; append `!` to refresh local cache
" " :PluginClean      - confirms removal of unused plugins; append `!` to
" auto-approve removal
" "
" " see :h vundle for more details or wiki for FAQ
" " Put your non-Plugin stuff after this line

set shell=zsh
